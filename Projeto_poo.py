import numpy as np
import random

class SemPerigo(Exception):
    def __init__(self):
        super().__init__('O Vírus não se propaga mais!!! ', end= '\U0001F604')
        '''fazer acumulador de turno'''

class Pessoa:
    def __init__(self,pos_y:int, pos_x:int, turno: int, condicao: int, identificador: int):
        self.__pos_y = None
        self.__pos_x = None
        self.__suscetivel = False
        self.__turno = 0
        self.__condicao = 0
        self.__identificador = identificador

    '''  condicao == 0: Vazio
         condicao == 1: Saudável
         condicao == 2: Infectado
         condicao == 3: Doente
         condicao == 4: Recuperado
         condicao == 5: Morto
    '''

# ---------- Getters and Setters ---------- #
    @property
    def pos_y(self):
        return self.__pos_y

    @pos_y.setter 
    def pos_y(self,pos_y):
        self.__pos_y = pos_y
    
    @property
    def pos_x(self):
            return self.__pos_x
            
    @pos_x.setter
    def pos_x(self,pos_x):
        self.__pos_x = pos_x
    
    @property
    def turno(self):
        return self.__turno

    @turno.setter
    def turno(self,turno):
        self.__turno = turno

    @property
    def condicao(self):
        return self.__condicao

    @condicao.setter
    def condicao(self,condicao):
        self.__condicao = condicao

    @property
    def identificador(self):
        return self.__identificador

    @identificador.setter
    def identificador(self,identificador):
        self.__identificador = identificador

#--------------------

class Saudavel(Pessoa):
    def __init__(self, condicao: int, pos_y, pos_x):
        self.condicao= 1

    def movimenta(self):
        super().__init__(pos_y, pos_x)
        moviment (pos_y,pos_x, random.randint(1,4))
        
        
class Infectado(Pessoa):
    def __init__(self, condicao: int):
        self.condicao = 2

    def movimenta(self):
        moviment (pos_y,pos_x, random.randint(1,4))
        

class Doente(Pessoa):
    def __init__(self, condicao: int):
        self.condicao = 3

    def movimenta(self, posicao:int):
        moviment (pos_y,pos_x, random.randint(1,4))

class Recuperado(Pessoa):
    def __init__(self, condicao: int):
        self.condicao = 4

    def movimenta(self, posicao: int):
        moviment (pos_y,pos_x, random.randint(1,4))

class Morto(Pessoa):
    def __init__(self, condicao: int, int):
        self.condicao = 5

    def movimenta(self, posicao: int):
        moviment (pos_y,pos_x, random.randint(1,4))
        
class Control:
    def __initi__(self, controle, qtd_pessoa: int, qtd_sauda: int, qtd_infec: int, qtd_doent: int, qtd_recup: int, qtd_morto: int):
        self.controle = np.empty((5,5001),dtype = int)
        self.qtd_pessoa = 0
        self.qtd_sauda = 0
        self.qtd_infec = 0
        self.qtd_doent = 0
        self.qtd_recup = 0
        self.qtd_morto = 0
'''
y0 = y
y1 = x
y2 = condicao
y3 = movimentado
y4 = turno

#18 turnos doente
#30 infectado
#90% de cura
'''
class Acm:
  def __init__(self,acumulador):
    self.acumulador = np.empty((5,1801), dtype = int)
    '''
    y0=Saudavel
    y1=Infectado
    y2=Doente
    y3=Recuperado
    y4=Morto
    '''

class Matriz:
    def __init__(self, my_array: Pessoa):
        self.m_array = np.empty((100,100), dtype = Pessoa) 
    

    t=Infectado()
    sau = Saudavel()
    
    
    while control.qtd_infec <=5:
        y = random.randint(0,99)
        x = random.randint(0,99)
        if m_array[y,x].pessoa.__condicao == 0:
            m_array[y,x] = t
            pessoa.__identificador = pessoa.__identificador +1
            control.qtd_infec += 1
            control.qtd_pessoa +=1
            controle[0,control.qtd_pessoa]= y
            controle[1,control.qtd_pessoa]= x
            controle[2,control.qtd_pessoa]= 2
            controle[3,control.qtd_pessoa]= 0
            controle[4,control.qtd_pessoa]= 0
            

    while control.qtd_sauda <=4995:
        y = random.randint(0,99)
        x = random.randint(0,99)
        if ((m_array[y,x].pessoa__condicao != 1) and (m_array[y,x].pessoa__condicao != 2)):
            m_array[y,x] = sau
            pessoa.__identificador = pessoa.__identificador +1
            control.qtd_sauda += 1
            control.qtd_pessoa +=1        
            controle[0,control.qtd_pessoa]= y
            controle[1,control.qtd_pessoa]= x
            controle[2,control.qtd_pessoa]= 1
            controle[3,control.qtd_pessoa]= 0
    

    print(m_array)
    
my_array = Matriz()
control = Control()
controle = Controle()
acumulador = Acm
acumulador[0,0]=4995
acumulador[1,0]=5
turno =0

def moviment (y,x,valor):
    if valor == 1: #down
        if y==0:
            if my_array[98, x] == 0:
                my_array[98, x] = my_array[y, x]
                my_array[y,x] = 0
            elif my_array[99,x] == 0:
                my_array[99, x] = my_array[98, x]
                my_array[98, x] = my_array[y, x]
                my_array[y,x] = 0
            else:
                aux = my_array[y, x]
                my_array[y, x] = my_array[99, x]
                my_array[99, x] = my_array[98 , x]
                my_array[98, x] = aux
        elif y==1:
            if my_array[99, x] == 0:
                my_array[99, x] = my_array[y, x]
                my_array[y,x] = 0
            elif my_array[0,x] == 0:
                my_array[0, x] = my_array[99, x]
                my_array[99, x] = my_array[y, x]
                my_array[y,x] = 0
            else:
                aux = my_array[y, x]
                my_array[y, x ] = my_array[0, x]
                my_array[0, x] = my_array[99 , x]
                my_array[99, x] = aux
        else:
            if my_array[y+2, x] == 0:
                my_array[y+2, x] = my_array[y, x]
                my_array[y,x] = 0
            elif my_array[y+1,x] == 0:
                my_array[y+1, x] = my_array[y+2, x]
                my_array[y+2, x] = my_array[y, x]
                my_array[y,x] = 0
            else:
                aux = my_array[y, x]
                my_array[y, x ] = my_array[y+1, x]
                my_array[y+1, x] = my_array[y+2 , x]
                my_array[y+2, x] = aux

    if valor == 2: #up
        if y==98:
            if my_array[0, x] == 0:
                my_array[0, x] = my_array[y, x]
                my_array[y,x] = 0
            elif my_array[1,x] == 0:
                my_array[1, x] = my_array[0, x]
                my_array[0, x] = my_array[y, x]
                my_array[y,x] = 0
            else:
                aux = my_array[y, x]
                my_array[y, x] = my_array[1, x]
                my_array[1, x] = my_array[0, x]
                my_array[0, x] = aux
        elif y==99:
            if my_array[1, x] == 0:
                my_array[99, x] = my_array[y, x]
                my_array[y,x] = 0
            elif my_array[0,x] == 0:
                my_array[0, x] = my_array[1, x]
                my_array[1, x] = my_array[y, x]
                my_array[y,x] = 0
            else:
                aux = my_array[y, x]
                my_array[y, x ] = my_array[0, x]
                my_array[0, x] = my_array[1 , x]
                my_array[1, x] = aux
        else:
            if my_array[y-2, x] == 0:
                my_array[y-2, x] = my_array[y, x]
                my_array[y,x] = 0
            elif my_array[y-1,x] == 0:
                my_array[y-1, x] = my_array[y-2, x]
                my_array[y-2, x] = my_array[y, x]
                my_array[y,x] = 0
            else:
                aux = my_array[y, x]
                my_array[y, x ] = my_array[y-1, x]
                my_array[y-1, x] = my_array[y-2 , x]
                my_array[y-2, x] = aux
    

    if valor == 3: #right
        if x==98:
            if my_array[y, 0] == 0:
                my_array[y, 0] = my_array[y, x]
                my_array[y,x] = 0
            elif my_array[y,99] == 0:
                my_array[y, 99] = my_array[y, 0]
                my_array[y, 0] = my_array[y, x]
                my_array[y,x] = 0
            else:
                aux = my_array[y, x]
                my_array[y, x] = my_array[y,99]
                my_array[y,99] = my_array[y, 0]
                my_array[y, 0] = aux
        elif x==99:
            if my_array[y, 1] == 0:
                my_array[y, 99] = my_array[y, x]
                my_array[y,x] = 0
            elif my_array[y, 0] == 0:
                my_array[y, 0] = my_array[y, 1]
                my_array[y, 1] = my_array[y, x]
                my_array[y,x] = 0
            else:
                aux = my_array[y, x]
                my_array[y, x ] = my_array[y, 0]
                my_array[y, 0] = my_array[y, 1]
                my_array[y, 1] = aux
        else:
            if my_array[y, x+2] == 0:
                my_array[y, x+2] = my_array[y, x]
                my_array[y,x] = 0
            elif my_array[y, x+1] == 0:
                my_array[y, x+1] = my_array[y,x+2]
                my_array[y, x+2] = my_array[y, x]
                my_array[y,x] = 0
            else:
                aux = my_array[y, x]
                my_array[y, x ] = my_array[y,x+1]
                my_array[y, x+1] = my_array[y, x+2]
                my_array[y, x+2] = aux    


    if valor == 4: #left
        if x==1:
            if my_array[y, 99] == 0:
                my_array[y, 99] = my_array[y, x]
                my_array[y,x] = 0
            elif my_array[y,0] == 0:
                my_array[y, 99] = my_array[y, 0]
                my_array[y, 0] = my_array[y, x]
                my_array[y,x] = 0
            else:
                aux = my_array[y, x]
                my_array[y, x] = my_array[y,0]
                my_array[y,0] = my_array[y, 99]
                my_array[y, 99] = aux
        elif x==0:
            if my_array[y, 98] == 0:
                my_array[y, 98] = my_array[y, x]
                my_array[y,x] = 0
            elif my_array[y, 99] == 0:
                my_array[y, 98] = my_array[y, 99]
                my_array[y, 99] = my_array[y, x]
                my_array[y,x] = 0
            else:
                aux = my_array[y, x]
                my_array[y, x ] = my_array[y, 99]
                my_array[y, 99] = my_array[y, 98]
                my_array[y, 98] = aux
        else:
            if my_array[y, x-2] == 0:
                my_array[y, x-2] = my_array[y, x]
                my_array[y,x] = 0
            elif my_array[y, x-1] == 0:
                my_array[y, x-1] = my_array[y,x-2]
                my_array[y, x-2] = my_array[y, x]
                my_array[y,x] = 0
            else:
                aux = my_array[y, x]
                my_array[y, x ] = my_array[y,x-1]
                my_array[y, x-1] = my_array[y, x-2]



def infe(self, r):
    r=random.randint(0,3)
    if r == 3:
        return 1
    else:
        return 2

def infectar (self, y,x):
    if y == 97:
        if x == 97:
            for j in (y-3,y+2):
                for k in(x-3,x+2):
                    my_array[1,j,k] = infe

            my_array[1,94,0]= infe
            my_array[1,95,0]= infe
            my_array[1,96,0]= infe
            my_array[1,97,0]= infe
            my_array[1,98,0]= infe
            my_array[1,99,0]= infe
            my_array[1,0,0]= infe
            my_array[1,0,94]= infe
            my_array[1,0,95]= infe
            my_array[1,0,96]= infe
            my_array[1,0,97]= infe
            my_array[1,0,98]= infe
            my_array[1,0,99]= infe

        elif x== 98:
            for j in (y-3,y+2):
                for k in(x-3,x+1):
                    my_array[1,j,k] = infe

            my_array[1,94,0]= infe                        
            my_array[1,95,0]= infe
            my_array[1,96,0]= infe
            my_array[1,97,0]= infe
            my_array[1,98,0]= infe
            my_array[1,99,0]= infe
            my_array[1,0,0]= infe
            my_array[1,94,1]= infe                        
            my_array[1,95,1]= infe
            my_array[1,96,1]= infe
            my_array[1,97,1]= infe
            my_array[1,98,1]= infe
            my_array[1,99,1]= infe
            my_array[1,0,1]= infe
            my_array[1,0,95]= infe
            my_array[1,0,96]= infe
            my_array[1,0,97]= infe
            my_array[1,0,98]= infe
            my_array[1,0,99]= infe

        elif x== 99:
            for j in (y-3,y+2):
                for k in(x-3,x):
                    my_array[1,j,k] = infe

            my_array[1,94,0]= infe                        
            my_array[1,95,0]= infe
            my_array[1,96,0]= infe
            my_array[1,97,0]= infe
            my_array[1,98,0]= infe
            my_array[1,99,0]= infe
            my_array[1,0,0]= infe
            my_array[1,94,1]= infe                        
            my_array[1,95,1]= infe
            my_array[1,96,1]= infe
            my_array[1,97,1]= infe
            my_array[1,98,1]= infe
            my_array[1,99,1]= infe
            my_array[1,0,1]= infe
            my_array[1,94,2]= infe                        
            my_array[1,95,2]= infe
            my_array[1,96,2]= infe
            my_array[1,97,2]= infe
            my_array[1,98,2]= infe
            my_array[1,99,2]= infe
            my_array[1,0,2]= infe
            my_array[1,0,96]= infe
            my_array[1,0,97]= infe
            my_array[1,0,98]= infe
            my_array[1,0,99]= infe

        else:
            for j in (y-3,y+2):
                for k in(x-3,x+3):
                    my_array[1,j,k] = infe
            for k in (x-3,x+3):
                my_array[1,0,k]= infe
            
    elif y == 98:
        if x == 97:
            for j in (y-3, y+1):
                for k in (x-3,x+2):
                    my_array[1,j,k] = infe
                my_array[1,j,0] = infe

            for j in (0,1):
                for k in (x-3,x+2):
                    my_array[1,j,k] = infe
                my_array[1,j,0] = infe

        if x == 98:
            for j in (y-3, y+1):
                for k in (x-3,x+1):
                    my_array[1,j,k] = infe
                my_array[1,j,0] = infe
                my_array[1,j,1] = infe

            for j in (0,1):
                for k in (x-3,x+2):
                    my_array[1,j,k] = infe
                my_array[1,j,0] = infe
                my_array[1,j,1] = infe
                
        elif x == 99:
            for j in (y-3, y+1):
                for k in (x-3,x):
                    my_array[1,j,k] = infe
                for k in (0,2):
                    my_array[1,j,k] = infe
            for j in (0,1):
                for k in (x-3,x):
                    my_array[1,j,k] = infe
                for k in (0,2):
                    my_array[1,j,k] = infe
        else:
            for j in (y-3,y+1):
                for k in(x-3,x+3):
                    my_array[1,j,k] = infe
            for j in (0,1):
                for k in (x-3,x+3):
                    my_array[1,j,k]= infe
    elif y == 99:
        if x == 97:
            for j in (y-3, y):
                for k in (x-3,x+2):
                    my_array[1,j,k] = infe
                my_array[1,j,0] = infe

            for j in (0,2):
                for k in (x-3,x+2):
                    my_array[1,j,k] = infe
                my_array[1,j,0] = infe

        elif x == 98:
            for j in (y-3, y):
                for k in (x-3,x+1):
                    my_array[1,j,k] = infe
                for k in (0,1):
                    my_array[1,j,k] = infe
            for j in (0,3):
                for k in (x-3,x+1):
                    my_array[1,j,k] = infe
                for k in (0,1):
                    my_array[1,j,k] = infe
                
                
        elif x == 99:
            for j in (y-3, y):
                for k in (x-3,x):
                    my_array[1,j,k] = infe
                for k in (0,2):
                    my_array[1,j,k] = infe
            for j in (0,2):
                for k in (x-3,x):
                    my_array[1,j,k] = infe
                for k in (0,2):
                    my_array[1,j,k] = infe
        else:
            for j in (y-3,y,0):
                for k in(x-3,x+3):
                    my_array[1,j,k] = infe
            for k in (x-3,x+3):
                my_array[1,0,k]= infe
                my_array[1,1,k]= infe
                my_array[1,1,k]= infe
    elif x == 97: 
        for j in (y-3, y+3):
            for k in (x-3, x+2):
                my_array[1,j,k] = infe
            my_array[1,j,0] = infe
    elif x == 98:
        for j in (y-3, y+3):
            for k in (x-3, x+1):
                my_array[1,j,k] = infe
            my_array[1,j,0] = infe
            my_array[1,j,1] = infe
    elif x == 99:
        for j in (y-3, y+3):
            for k in (x-3, x+1):
                my_array[1,j,k] = infe
            my_array[1,j,0] = infe
            my_array[1,j,1] = infe
            my_array[1,j,2] = infe
    else:
        for j in (y-3,y+3):
            for k in (x-3,x+3):    
                my_array[1,j,k] = infe


def andamento100():
    for i in (1, 5000):
        if (controle[3,i] == 1 or controle[3,i] == 2):
            moviment (controle[1,i],controle[2,i], random.randint(1,4))
    for i in (1, 5001):
      if((controle[2,i]==2) or (controle[2,i]==3)):
        controle[4,i] = controle[4,i] + 1
      if ((controle[2,i]==2) and controle[4,i]>30):
        controle[2,i]=3
        controle[4,i]=0
        my_array[controle[0,i],controle[1,i]].pessoa__condicao = 3 
      
      if ((controle[2,i]==3) and controle[4,i]>18):
        w = ramdom.randint(0,9) 
        if w==9:
            controle[2,i]=5
            my_array[controle[0,i],controle[1,i]].pessoa__condicao = 5
        else:
            controle[2,i]=4
            my_array[controle[0,i],controle[1,i]].pessoa__condicao = 4
      if (controle[2,i]== 5):
            my_array[controle[0,i],controle[1,i]].pessoa__condicao = 0
    if ((control.qtd_infec + control.qtd_doent)==0):
        raise SemPerigo
    finalizar
        
def andamento50():
    while q < ((Pessoa.quantidade)*50/100):
        i = random.randint(1,5000)
        
        if ((controle[3,i] == 1 or controle[3,i] == 2) and controle[4,i]==0):
            moviment (controle[1,i],controle[2,i], random.randint(1,4))
            controle[4,1]= 1
    controle[4,:] = 0

    for i in (1, 5001):
      if((controle[2,i]==2) or (controle[2,i]==3)):
        controle[4,i] = controle[4,i] + 1
      
      if ((controle[2,i]==2) and controle[4,i]>30):
        controle[2,i]=3
        controle[4,i]=0
        my_array[controle[0,i],controle[1,i]].pessoa__condicao = 3 
      
      if ((controle[2,i]==3) and controle[4,i]>18):
        w = ramdom.randint(0,9) 
       
        if w==9:
            controle[2,i]=5
            my_array[controle[0,i],controle[1,i]].pessoa__condicao = 5
        
        else:
            controle[2,i]=4
            my_array[controle[0,i],controle[1,i]].pessoa__condicao = 4
      
      if (controle[2,i]== 5):
            my_array[controle[0,i],controle[1,i]].pessoa__condicao = 0

    if ((control.qtd_infec + control.qtd_doent)==0):
        raise SemPerigo
    finalizar
    
def andamento20():

    while q < ((Pessoa.quantidade)*20/100):
        i = random.randint(1,5000)
        
        if ((controle[3,i] == 1 or controle[3,i] == 2) and controle[4,i]==0):
            moviment (controle[1,i],controle[2,i], random.randint(1,4))
            controle[4,1]= 1
    controle[4,:] = 0

    for i in (1, 5001):
      if((controle[2,i]==2) or (controle[2,i]==3)):
        controle[4,i] = controle[4,i] + 1
      
      if ((controle[2,i]==2) and controle[4,i]>30):
        controle[2,i]=3
        controle[4,i]=0
        my_array[controle[0,i],controle[1,i]].pessoa__condicao = 3 
      
      if ((controle[2,i]==3) and controle[4,i]>18):
        w = ramdom.randint(0,9) 
        
        if w==9:
            controle[2,i]=5
            my_array[controle[0,i],controle[1,i]].pessoa__condicao = 5
        else:
            controle[2,i]=4
            my_array[controle[0,i],controle[1,i]].pessoa__condicao = 4
      if (controle[2,i]== 5):
            my_array[controle[0,i],controle[1,i]].pessoa__condicao = 0

    if ((control.qtd_infec + control.qtd_doent)==0):
        raise SemPerigo  
    finalizar

def finalizar():
    turno += turno m 
    acumulador[4,turno]= control.qtd_morto - acumulador[4,turno-1] 
    acumulador[3,turno]= control.qtd_recup - acumulador[3,turno-1] 
    acumulador[2,turno]= control.qtd_doent - acumulador[4,turno-1] 
    acumulador[1,turno]= control.qtd_infec - acumulador[4,turno-1] 
    acumulador[0,turno]= control.qtd_sauda - acumulador[4,turno-1] 
    